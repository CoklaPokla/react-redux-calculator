import React from "react";
import { connect } from "react-redux";
import { display,clear,evaluate, toggle_theme } from "../redux/actions";

class Panel extends React.Component {
  constructor(props) {
    super(props);
    this.state = { input: "0", theme: "dark" ,flag:0, mode:"normal"}
  }
  
  componentDidMount() {
      var _this2 = this; // Creating a local copy to not lose "this" instance

      // Based on initial configs set background color
      let color = _this2.state.theme=="normal"?"#fff":"black";
      document.body.style = "background:"+color;

      //Start by evaluating 0
      _this2.props.evaluate("0")

      // Add event listener to get number inputs from keyboard for the calculator and backspace for edits
      document.addEventListener("keydown", function (e) {
        if (!isNaN(e.key) || ['+', '-', '*', '/', '='].includes(e.key)) {
          e.preventDefault()
          _this2.updateInput(_this2.state.input + e.key);
        }
        else if(e.keyCode==8){ // Backspace captured
          _this2.updateInput(_this2.state.input.slice(0,-1));
        }
      },
      false);

  }

  updateInput = input => {
    // Updates the screen and result variables according to the incoming character

    if(['$', '['].includes(input.slice(-1))){ // Scientific mode '$' represents Math.sqrt and '[' represents Math.log
      if (input.slice(-1)=="$"){
        this.props.evaluate(Math.sqrt(input.slice(0,-1)));
        this.setState({input:""}) // Reset input once operation is complete
      }
      else{
        this.props.evaluate(Math.log(input.slice(0,-1)));
        this.setState({input:""})
      }
      
    }
    
    else if (['+', '-', '/', '*', '='].includes(input.slice(-1))) { 
      // Compute when a new operator comes in the input
      
      if (input.length<=2 && isNaN(input[0])){ // Handling consecutive operators by resetting
          this.resetCalc()
      }
      else{
          this.props.display(input); // Send in the input and reset
          this.setState({ input: "" });
      }
        
    } 
    else {
      // Display current head/result 
        if (this.state.input === "0") { // Handling leading zeroes
          input = input.slice(1);
        }
        this.props.evaluate(input);
        this.setState({ input: input });
    }
  };

  handleFinal = () => {
    this.props.evaluate();
    this.setState({ input: "i" });
  };


  resetCalc = () => {
    // Resets all variables 
      this.props.clear();
      this.setState({ input: "" });
      // this.props.evaluate()
  };

  toggle =()=> {
    // Toggles between normal and dark theme
      let color = this.state.theme=="normal"?"black":"white";
      document.body.style = "background:"+color; 
      this.props.toggle_theme();
      this.setState({ theme: this.state.theme == "normal" ? "dark" : "normal" });
  };

  toggle_mode = () =>{
    // Toggles Scientific mode
    this.setState({mode:this.state.mode=="normal"?"scientific":"normal"})
  }
  numberAdd = (e) => {
    // Handler for keypad clicks
        try{
          this.updateInput(this.state.input + e.currentTarget.attributes.value.value);
        }
        catch(err){
          this.updateInput("0");
          throw(err)
        }      
  };

  render() {
    return (
      <div>
        <input className="hidden"
          onChange={e => this.updateInput(e.target.value)}
          value={this.state.input} 
        />
        
        <table className={this.state.theme}>
        <tbody>
          <tr>
            <td value="1" onClick={this.numberAdd}>1</td>
            <td value="1" onClick={this.numberAdd}>2</td>
            <td value="1" onClick={this.numberAdd}>3</td>
            <td value="+" onClick={this.numberAdd}>+</td>
          </tr>
          <tr>
            <td value="4" onClick={this.numberAdd}>4</td>
            <td value="5" onClick={this.numberAdd}>5</td>
            <td value="6" onClick={this.numberAdd}>6</td>
            <td value="-" onClick={this.numberAdd}>-</td>
          </tr>
          <tr>
            <td value="7" onClick={this.numberAdd}>7</td>
            <td value="8" onClick={this.numberAdd}>8</td>
            <td value="9" onClick={this.numberAdd}>9</td>
            <td value="*" onClick={this.numberAdd}>*</td>
          </tr>
          <tr>
            <td value="0" onClick={this.numberAdd}>0</td>
            <td value="" onClick={this.resetCalc}>AC</td>
            <td value="" onClick={this.toggle}>Theme</td>
            <td value="/" onClick={this.numberAdd}>/</td>
          </tr>
          <tr>
            <td onClick={this.toggle_mode}>Mode</td>
            <td value="=" onClick={this.numberAdd}>=</td>
    {this.state.mode=="scientific" && (<td value="$" onClick={this.numberAdd}>Sqrt</td>) }
    {this.state.mode=="scientific" && (<td value="[" onClick={this.numberAdd}>LOG</td>) }
          
          </tr>
        </tbody>
        </table>
      </div>
    );
  }
}

export default connect(
  null,
  { display,clear,evaluate,toggle_theme }
)(Panel);

