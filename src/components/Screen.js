import React from "react";
import { connect } from "react-redux";
import { getCalcstring, getTheme} from "../redux/selectors";

const Screen = ({result,theme}) => (
  <div className={theme}>
  <span >{result}</span>
  <br></br>
  </div>
  
);
        
const mapStateToProps = state => {

  const result = getCalcstring(state);
  const theme = getTheme(state)
  return { result,theme }

};

export default connect(mapStateToProps)(Screen);
