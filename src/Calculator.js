import React from "react";
import DisplayCalc from "./components/DisplayCalc";
import Screen from "./components/Screen"
import "./styles.css";

export default function Calculator() {
  return (
    <div className="calculator">
      <table >
        <tbody>
          <tr>
            <td><Screen /></td></tr>
          <tr><td><DisplayCalc /></td></tr>
        </tbody>
      </table>
      <br></br><br></br>
      <p className="note">USE BACKSPACE TO EDIT</p>
      
    </div>
  )
}
