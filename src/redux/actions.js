import { CALC, EVAL, CLEAR, SET_FILTER, TOGGLE_THEME} from "./actionTypes";



export const display = content => ({
  type: CALC,
  payload: {
    content
  }
});

export const evaluate = content =>({
  type: EVAL,
  payload : {
    content
  }
})
export const clear = content =>({
  type: CLEAR
})
export const toggle_theme = content =>({
  type: TOGGLE_THEME
})



export const setFilter = filter => ({ type: SET_FILTER, payload: { filter } });
