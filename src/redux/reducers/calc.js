import { CALC, EVAL, CLEAR, TOGGLE_MODE, TOGGLE_THEME } from "../actionTypes";

const initialState = {
  allIds: [],
  byIds: {},
  calcString :"",
  result:"",
  op:"",
  theme:"dark",
  mode:"normal"
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CALC: {
      const { content } = action.payload;
      console.log("Calc recieved"+content)

      return {
        ...state,
        calcString : eval(state.result+state.op+content.slice(0,-1)),
        result : eval(state.result+state.op+content.slice(0,-1)),
        op:content.slice(-1)
      };
    }
    case CLEAR: {
      console.log("CLR received")
      return {
        ...state,
        calcString : "",
        result : "",
        op:""
      };
    }
    case EVAL: {
      var { content } = action.payload;
      console.log("EVAL recieved"+content)
      return {
        ...state,
        calcString:content
      }
    


  
/*

      if(op1!=""){
        if(op!=""){
          op2=content
          return {
            ...state,
            result : eval(op1+op+op2),
            calcString: content
            //
          };
        }
        else{
          op=""
        }

      }
*/
      // var pattern = /^[0-9|+|-|*|/]*/
      // // let temp = content
      // let eval_str  = state.calcString.slice(0,-1)
      
      // if (eval_str.match(pattern)){
      //   content = eval(eval_str)
      //   // temp = content
      // }
      // else{
      //   content = state.calcString
      // }
      
    }
    case TOGGLE_THEME: {
      return {
        ...state,
        theme : state.theme=="normal"?"dark":"normal"
      }
    }
    case TOGGLE_MODE: {
      return {}
    } 
    default:
      return state;
  }
}
