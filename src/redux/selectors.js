
export const getCalcstring = (store) => {
  // const calcstring = calcstring;
  return store.calc.calcString
};

export const getResult = (store) =>{
  return store.calc.result?store.calc.result:0
}
export const getTheme = (store) =>{
  return store.calc.theme
}