export const CALC = "CALC";
export const EVAL = "EVAL";
export const SET_FILTER = "SET_FILTER";
export const CLEAR = "CLEAR";
export const TOGGLE_THEME = "TOGGLE_THEME";
export const TOGGLE_MODE = "TOGGLE_MODE";
